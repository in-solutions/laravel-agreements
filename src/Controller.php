<?php

namespace Insolutions\Agreements;
 
use Illuminate\Http\Request;

use Insolutions\Auth\MiddlewareOnlyAuth as MiddlewareOnlyAuth;

use Insolutions\Agreements\Agreement;
use App\User;
use Auth;

class Controller extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
    	// require ins/auth/MiddlwwareOnlyAuth middleware for all actions except "getNewest"
        $this->middleware(MiddlewareOnlyAuth::class)->except(['getNewest','acceptByHash']);
    }

    public function acceptByHash(Request $r, $hash) {
		list($user_id, $agreement_id) = Agreement::decodeUserAcceptLinkHash($hash);

		// controller part
		$user = User::find($user_id);
		if (!$user) {
			\Log::warning("User '{$user_id}' not found");
		}

		$agreement = Agreement::find($agreement_id);
		if (!$agreement) {
			\Log::warning("Agreement '{$agreement_id}' not found");
		} else {
			$agreement->acceptByUser($user);
		}

		redirect($r->continue);
	}

	public function getNewest(Request $r, $type) {
		return response()->json(Agreement::getNewestOfType($type));
	}

	public function getNewestStatus(Request $r, $type) {
		$user = Auth::user();		

		$agreement = Agreement::getNewestOfType($type);
		
		if (!$agreement) {
			return response("No agreement of type {$type} found!", 404);
		}

		return response()->json([
			'status' => $this->agreementUser_status($agreement, $user),
			'agreement' => $agreement
		]);
	}

	public function status($agreement_id) {
		$user = Auth::user();
		
		return response()->json(
			$this->agreementUser_status(
				Agreement::findOrFail($agreement_id),
				$user
			)
		);
	}

	private function agreementUser_status(Agreement $agr, User $user) {
		return [
			'accepted_at' => $agr->whenAcceptedByUser($user)
		];
	}

	public function accept(Request $r, $agreement_id) {
		$user = Auth::user();

		$agr = Agreement::findOrFail($agreement_id);

		$agr->acceptByUser($user);
	}

	public function unaccept(Request $r, $agreement_id) {
		$user = Auth::user();

		$agr = Agreement::findOrFail($agreement_id);

		$agr->unacceptByUser($user);
	}
}