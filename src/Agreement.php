<?php

namespace Insolutions\Agreements;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use Carbon\Carbon;

class Agreement extends Model
{
    use SoftDeletes;

    protected $table = "t_agreement"; 

    public static function getNewestOfType($type = null) {
    	return self::where('type', $type)->orderBy('created_at', 'desc')->first();
    }

    public function generateUserAcceptLink(User $user) {
        return route('link_agreement_accept_by_hash', 
            ['hash' => base64_encode($user->id . ',' . $this->id . ',' . uniqid())]
        );
    }

    public static function decodeUserAcceptLinkHash($hash) {
        $data = explode(',', base64_decode($hash));
        return [$data[0], $data[1]];    // userId, agreementId
    }

    public function whenAcceptedByUser(User $user) {
        $relation = $this->getUserRelation($user);
        return $relation
            ? $relation->pivot['accepted_at'] // if response is not empty
            : null;    // if response is empty (null relation)
    }

    public function getUserRelation(User $user) {
        return $this->users()->wherePivot('user_id', $user->id)->first();
    }

    public function users() {
        return $this->belongsToMany('App\User', 't_agreement_user', 'agreement_id', 'user_id')
            ->withPivot('required', 'accepted_at');
    	//return $this->hasManyThrough('App\User', 'Insolutions\Agreements\AgreementUser', 'user_id', 'id', 'id', 'agreement_id');
    }

    public function acceptByUser(User $user) {
        // adds entry if not exists, if exists update accepted_at
        $this->users()->syncWithoutDetaching([$user->id => ['accepted_at' => Carbon::now()]]);

        // TODO: (idea) check if agreement-user is not accepted already (to not update timestamp if already present)
    }

    public function unacceptByUser(User $user) {
        // sets accepted_at null if existing entry agreement-user, if not exists, do not create it
        $this->users()->updateExistingPivot($user->id, ['accepted_at' => null]); 
        $this->deleteNonRequiredPivots();
    }

    private function deleteNonRequiredPivots() {
        $this->users()->wherePivot('accepted_at', null)->wherePivot('required', null)->detach();
    }
}
