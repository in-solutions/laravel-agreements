<?php

Route::group(['prefix' => 'agreements'], function () {
	
	Route::get('type/{type}/newest', 'Insolutions\Agreements\Controller@getNewest');
	Route::get('type/{type}/newest/status', 'Insolutions\Agreements\Controller@getNewestStatus');

	Route::get('accept/{hash}', 'Insolutions\Agreements\Controller@acceptByHash')->name('link_agreement_accept_by_hash');

	Route::post('agreement/{agreement_id}/accept', 'Insolutions\Agreements\Controller@accept');
	Route::post('agreement/{agreement_id}/unaccept', 'Insolutions\Agreements\Controller@unaccept');
	Route::get('agreement/{agreement_id}/status', 'Insolutions\Agreements\Controller@status');
});